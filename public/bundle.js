(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

// Canvas
const CANVAS_WIDTH = 161.8
const CANVAS_HEIGHT = 100
const CANVAS_HALF_WIDTH = CANVAS_WIDTH / 2
const CANVAS_HALF_HEIGHT = CANVAS_HEIGHT / 2
const CANVAS_HALF_SIZE = {
  x: CANVAS_HALF_WIDTH, y: CANVAS_HALF_HEIGHT
}

// World
const WORLD_WIDTH = 500
const WORLD_HEIGHT = 500
const WORLD_HALF_WIDTH = WORLD_WIDTH / 2
const WORLD_HALF_HEIGHT = WORLD_HEIGHT / 2
const WORLD_SIZE = { x: WORLD_WIDTH, y: WORLD_HEIGHT }
const WORLD_HALF_SIZE = { x: WORLD_HALF_WIDTH, y: WORLD_HALF_HEIGHT }

// Colors
const RED = '#FF0000'
const SKY_BLUE = '#0088FF'
const BLUE = '#0000FF'
const BLACK = '#000000'
const GREEN = '#008000'
const YELLOW = '#FFFF00'
const PURPLE = '#7F007F'
const FIRE_YELLOW = '#E5AD23'
const WHITE = '#FFFFFF'
const DARK_GRAY = '#a9a9a9'
// Update
const UPDATE_RATE = 50
const UPDATE_RATE_MODIFIER = 1
const UPDATED_RATE = UPDATE_RATE / UPDATE_RATE_MODIFIER
const UPDATE_SECOND = 1000 / UPDATED_RATE

// Size
const MINIMUM_SIZE = 5
const MINIMUM_RADIUS = MINIMUM_SIZE / 2

// Velocity
const MAXIMUM_SPEED = (MINIMUM_SIZE / 5) * UPDATE_RATE_MODIFIER
const MINIMUM_SPEED = (MAXIMUM_SPEED / 2)

// Math
const TWO_PI = Math.PI * 2
const ZERO_VECTOR = { x: 0, y: 0 }

// Debug
const DEBUG = false

const add = (a, b) => a + b
const subtract = (a, b) => a - b
const difference = (a, b) => Math.abs(a - b)
const square = (a) => a * a

const vectorDo = (vector, fn) => (
  { x: fn(vector.x), y: fn(vector.y) }
)
const vectorDoTwo = (a, b, fn) => (
  { x: fn(a.x, b.x), y: fn(a.y, b.y) }
)
const vectorDoThree = (a, b, c, fn) => (
  { x: fn(a.x, b.x, c.x), y: fn(a.y, b.y, c.y) }
)

const vectorAdd = (a, b) => vectorDoTwo(a, b, add)
const vectorSubtract = (a, b) => vectorDoTwo(
  a, b, subtract
)
const vectorDifference = (a, b) => vectorDoTwo(
  a, b, difference
)

const times = (fn, number, current = 0) => {
  if (current < number) {
    fn()
    times(fn, number, current + 1)
  }
}

const getLength = (vector) => Math.sqrt(square(vector.x) + square(vector.y))

const getDistance = (a, b) => getLength(
  vectorSubtract(a, b)
)

const getUnitVector = (vector) => {
  const vectorLength = getLength(vector)
  if (vectorLength === 0) return vector

  return vectorDo(vector, (value) => value / vectorLength)
}

const getDirection = (angle) => (
  { x: Math.cos(angle), y: Math.sin(angle) }
)

const getSequence = (start, end, step) => {
  const array = []
  array[0] = start
  while (array[array.length - 1] < end) {
    array[array.length] = array[array.length - 1] + step
  }
  if (array[array.length - 1] !== end) {
    array[array.length] = end
  }
  return array
}

const getRay = (point, angle, sequence) => {
  const directionVector = getDirection(angle)
  return sequence.map((element) => vectorDoTwo(
    point, directionVector, (a, b) => a + element * b
  ))
}

const rectangleCollision = (a, b) => {
  const difference = vectorDifference(a.position, b.position)
  const violation = vectorDoThree(
    a.halfSize,
    b.halfSize,
    difference,
    (a, b, c) => a + b - c
  )

  if (violation.x > 0 && violation.y > 0) {
    return violation
  } else {
    return null
  }
}

const getRandomPosition = (vector) => vectorDo(
  vector, (a) => Math.random() * a - (a / 2)
)

const getSkeleton = (position, halfSize) => {
  const points = [
    // Upper left
    { x: position.x - halfSize.x, y: position.y + halfSize.y },
    // Upper right
    { x: position.x + halfSize.x, y: position.y + halfSize.y },
    // Lower left
    { x: position.x - halfSize.x, y: position.y - halfSize.y },
    // Lower Right
    { x: position.x + halfSize.x, y: position.y - halfSize.y }
  ]
  return {
    points,
    segments: [
      { a: points[0], b: points[1] }, // Top
      { a: points[1], b: points[3] }, // Right
      { a: points[2], b: points[3] }, // Bottom
      { a: points[0], b: points[2] } // Left
    ]
  }
}

const lerp = (a, b, weight) => (
  weight * a + (1 - weight) * b
)
const lerpVector = (a, b, weight) => (
  vectorDoTwo(
    a, b, (a, b) => weight * a + (1 - weight) * b
  )
)

const getAngle = (a, b) => Math.atan2(b.y - a.y, b.x - a.x)

// TODO: check name
const getUnitRay = (a, angle) => ({
  a: a,
  b: vectorAdd(a, { x: Math.cos(angle), y: Math.sin(angle) })
})

const getIntersection = (ray, segment) => {
  // Are they parallel? If so, no intersect
  const rayVector = getUnitVector(vectorSubtract(ray.b, ray.a))
  const segmentVector = vectorSubtract(segment.b, segment.a)
  const rayLength = getLength(rayVector)
  const segmentLength = getLength(segmentVector)

  // TODO: Break into variables and use vectorDo
  if (
    rayVector.x / rayLength ===
    segmentVector.x / segmentLength &&
    rayVector.y / rayLength ===
    segmentVector.y / segmentLength
  ) {
    // Unit vectors are the same.
    return null
  }

  // TODO: Break into variables
  // Solve for t1 and t2
  const t2 = (
    (
      rayVector.x * (segment.a.y - ray.a.y) +
      rayVector.y * (ray.a.x - segment.a.x)
    ) /
    (
      segmentVector.x * rayVector.y -
      segmentVector.y * rayVector.x
    )
  )

  // TODO: Break into variables
  const t1 = (
    segment.a.x + segmentVector.x * t2 - ray.a.x
  ) / rayVector.x

  // TODO: specify 'whatevers'
  // Must be within parametic whatevers for RAY/SEGMENT
  if (t1 < 0) return null
  if (t2 < 0 || t2 > 1) return null

  // Return the point of intersection
  return {
    x: ray.a.x + rayVector.x * t1,
    y: ray.a.y + rayVector.y * t1,
    param: t1
  }
}

const isIntersected = (a, b, segments) => {
  const angle = getAngle(a, b)
  const ray = getUnitRay(a, angle)
  const distance = getLength(vectorSubtract(a, b))

  return segments.some(segment => {
    const intersection = getIntersection(ray, segment)
    return intersection && intersection.param < distance
  })
}

const isVisible = (a, b, angle, radius, segments) => {
  const centerIntersected = isIntersected(a, b, segments)
  if (!centerIntersected) { return true }

  const rightDirection = getDirection(
    angle + Math.PI / 2
  )
  const rightOffset = vectorDo(rightDirection, a => a * radius)
  const rightPosition = vectorAdd(b, rightOffset)
  const rightIntersected = isIntersected(
    a, rightPosition, segments
  )
  if (!rightIntersected) { return true }

  const leftOffset = vectorDo(rightOffset, a => a * -1)
  const leftPosition = vectorAdd(b, leftOffset)
  const leftIntersected = isIntersected(
    a, leftPosition, segments
  )
  if (!leftIntersected) { return true }

  return false
}

const hitCheckPoint = (point, circles, rectangles) => {
  const circle = circles.find(
    (circle) => (
      getDistance(circle.position, point) < circle.radius
    )
  )
  return circle || rectangles.find((rectangle) => {
    const difference = vectorDifference(
      point, rectangle.position
    )
    const violation = vectorSubtract(
      rectangle.halfSize, difference
    )
    return violation.x > 0 && violation.y > 0
  })
}

const rayCast = (ray, circles, rectangles) => {
  const pointer = {
    point: ray[ray.length - 1],
    object: null
  }
  ray.some((point) => {
    const hit = hitCheckPoint(point, circles, rectangles)
    if (hit) {
      pointer.point = point
      pointer.object = hit

      return true
    }
  })

  return pointer
}

const rectangleBounce = (violation, a, b) => {
  if (violation.y > violation.x) {
    if (a.position.x > b.position.x) {
      if (!a.fixed) a.position.x += MAXIMUM_SPEED
      if (!b.fixed) b.position.x -= MAXIMUM_SPEED
    } else {
      if (!a.fixed) a.position.x -= MAXIMUM_SPEED
      if (!b.fixed) b.position.x += MAXIMUM_SPEED
    }
  } else {
    if (a.position.y > b.position.y) {
      if (!a.fixed) a.position.y += MAXIMUM_SPEED
      if (!b.fixed) b.position.y -= MAXIMUM_SPEED
    } else {
      if (!a.fixed) a.position.y -= MAXIMUM_SPEED
      if (!b.fixed) b.position.y += MAXIMUM_SPEED
    }
  }
}

const circleRectangleBounce = (circle, rectangle) => {
  const difference = vectorDifference(
    circle.position, rectangle.position
 )
  const violation = vectorDoTwo(
    rectangle.halfSize,
    difference,
    (a, b) => circle.radius + a - b
  )
  const sideCase = (
    Math.max(violation.x, violation.y) > circle.radius
  )
  const sideCollision = (
    violation.x > 0 && violation.y > 0 && sideCase
  )

  const diagonal = {
    x: rectangle.halfSize.x,
    y: -rectangle.halfSize.y
  }
  const corners = [
    vectorAdd(rectangle.position, diagonal),
    vectorSubtract(rectangle.position, diagonal),
    vectorAdd(rectangle.position, rectangle.halfSize),
    vectorSubtract(rectangle.position, rectangle.halfSize)
  ].map(corner => getDistance(corner, circle.position))
  const cornerDistance = Math.min(...corners)

  const collision = (
    sideCollision || (cornerDistance < circle.radius)
  )

  if (collision) {
    rectangleBounce(violation, circle, rectangle)
  }
}

const getClosest = (a, objects) => {
  const distances = objects.map(
    (object, index) => ({
      distance: getDistance(
        a.position, object.position
      ),
      index: index
    })
  )
  const closest = distances.sort(
    (a, b) => a.distance - b.distance
  )[0]

  return {
    object: objects[closest.index],
    distance: closest.distance
  }
}

const rectangles = []
const fixedRectangles = []
const unfixedRectangles = []

const createRectangle = (
  x,
  y,
  color,
  width,
  height,
  fixed = false
) => {
  const rectangle = createObject(x, y, color, fixed)
  rectangle.width = width
  rectangle.halfWidth = width / 2
  rectangle.height = height
  rectangle.halfHeight = height / 2
  rectangle.size = { x: width, y: height }
  rectangle.halfSize = { x: rectangle.halfWidth, y: rectangle.halfHeight }

  const skeleton = getSkeleton(
    rectangle.position, rectangle.halfSize
  )
  rectangle.points = skeleton.points
  rectangle.segments = skeleton.segments

  rectangles.push(rectangle)
  fixed ? fixedRectangles.push(rectangle) : unfixedRectangles.push(rectangle)
  return rectangle
}

const createRandomRectangle = () => {
  const position = getRandomPosition(WORLD_SIZE)
  const width = Math.random() * 100 + MINIMUM_SIZE
  const height = Math.random() * 100 + MINIMUM_SIZE

  const maximumSize = MINIMUM_SIZE * 2

  // Check if there is an overlap
  const skeleton = {
    position,
    halfSize: {
      x: (width / 2) + maximumSize,
      y: (height / 2) + maximumSize
    }
  }

  const overlap = rectangles.some(
    rectangle => rectangleCollision(skeleton, rectangle)
  )

  if (overlap) {
    return createRandomRectangle()
  } else {
    return createRectangle(position.x, position.y, PURPLE, width, height, true)
  }
}

const objects = []
const fixedObjects = []
const unfixedObjects = []
const createObject = (x, y, color, fixed = false) => {
  const object = {
    angle: 0,
    color,
    draw: { x, y },
    fixed,
    movementRate: MINIMUM_SPEED,
    position: { x, y },
    velocity: ZERO_VECTOR
  }

  objects.push(object)
  fixed ? fixedObjects.push(object) : unfixedObjects.push(object)
  return object
}

const createRandomObject = () => {
  const shape = Math.random()
  if (shape > 0.5) {
    return createRandomCircle()
  } else {
    return createRandomRectangle()
  }
}

const circles = []
const fixedCircles = []
const unfixedCircles = []

const createCircle = (
  x, y, color, radius, angle, fixed = false
) => {
  const circle = createObject(x, y, color, fixed)
  circle.angle = angle
  circle.brain = {
    ai: { name: 'civilian' },
    input: {
      left: false,
      right: false,
      forward: false,
      backward: false,
      aim: false,
      fire: false,
      strafeLeft: false,
      strafeRight: false
    }
  }
  circle.drawAngle = angle
  circle.radius = radius
  circle.visibleCircles = []
  circle.weaponSize = circle.radius + MAXIMUM_SPEED

  circles.push(circle)
  if (fixed) {
    fixedCircles.push(circle)
  } else {
    unfixedCircles.push(circle)
  }

  return circle
}

const createRandomCircle = () => {
  const position = getRandomPosition(WORLD_SIZE)
  const radius = (
    (Math.random() * MINIMUM_RADIUS) + MINIMUM_RADIUS
  )
  const angle = Math.random() * 2 * Math.PI

  return createCircle(
    position.x, position.y, BLUE, radius, angle
  )
}

/* global Audio */

const aimSound = new Audio(
  './public/sound/holdBreath.mp3'
)
const leeEnfieldSound = new Audio(
  './public/sound/leeEnfield.mp3'
)
const leeEnfieldReloadSound = new Audio(
  './public/sound/reload.wav'
)
const misfireSound = new Audio(
  './public/sound/misfire.wav'
)
const hitSound = new Audio(
  './public/sound/hit.mp3'
)
const civilianHitSound = new Audio(
  './public/sound/civilianHit.mp3'
)

const roamers = []
const roamerAi = {
  roamAngle: 0,
  getAngle () { return Math.random() * Math.PI * 2 },
  getTime () { return Math.random() * UPDATE_SECOND * 30 },
  name: 'roamer',
  roamTime: 0
}
const createRoamer = () => {
  const roamer = createRandomCircle()
  roamer.brain.ai = Object.assign({}, roamerAi)
  roamer.angle = roamer.brain.ai.getAngle()
  roamer.color = GREEN
  roamer.movementRate = MINIMUM_SPEED + Math.random() * MINIMUM_SPEED

  roamers.push(roamer)
  return roamer
}

const leeEnfield = {
  accuracy: 3,
  cooldown: UPDATE_SECOND * 2,
  charge: 0,
  hit: {},
  ray: [],
  reloadSound: leeEnfieldReloadSound,
  reloadSoundLength: 1265,
  remaining: 0,
  attackSound: leeEnfieldSound,
  time: 0
}
leeEnfield.playReloadSound = () => setTimeout(
  () => leeEnfield.reloadSound.play(),
  (leeEnfield.cooldown * UPDATED_RATE) - leeEnfield.reloadSoundLength
)

const combatants = []
const player = createCircle(0, 0, SKY_BLUE, MINIMUM_SIZE * 0.75, 0)
player.attack = Object.assign({}, leeEnfield)
player.brain.ai = { name: 'player' }
player.score = 0
player.movementRate = MAXIMUM_SPEED * 0.75
combatants.push(player)

const hunter = createCircle(0, 0, RED, MINIMUM_SIZE * 0.75, 0)
hunter.attack = Object.assign({}, leeEnfield)
hunter.brain.ai = Object.assign(roamerAi, { name: 'hunter', aimTime: 0 })
hunter.score = 0
hunter.movementRate = MAXIMUM_SPEED * 0.75
combatants.push(hunter)

const houseSize = MINIMUM_SIZE * 10
const halfHouseSize = houseSize / 2
createRectangle(
  halfHouseSize - MINIMUM_RADIUS,
  halfHouseSize + MINIMUM_SIZE * 2,
  PURPLE,
  houseSize,
  houseSize,
  true
) // The House

const WALL_COLOR = PURPLE
const WALL_SIZE = 10
// Left wall
createRectangle(
  -WORLD_HALF_WIDTH + 5,
  0,
  WALL_COLOR,
  WALL_SIZE,
  WORLD_HEIGHT,
  true
)

// Right wall
createRectangle(
  WORLD_HALF_WIDTH + 5,
  0,
  WALL_COLOR,
  WALL_SIZE,
  WORLD_HEIGHT,
  true
)

// Top wall
createRectangle(
  0,
  WORLD_HALF_HEIGHT + 5,
  WALL_COLOR,
  WORLD_WIDTH + 20,
  10,
  true
)

// Bottom wall
createRectangle(
  0,
  -WORLD_HALF_HEIGHT - 5,
  WALL_COLOR,
  WORLD_WIDTH + 20,
  10,
  true
)

times(createRandomObject, 50)
times(createRoamer, 25)

// Initialize the camera
const camera = {
  angle: 0,
  position: {
    x: 50,
    y: 50
  },
  subject: player
}
camera.angle = camera.subject.angle

const worldSkeleton$1 = getSkeleton(ZERO_VECTOR, WORLD_HALF_SIZE)
const rectangleSegments$1 = rectangles.map(
  rectangle => rectangle.segments
)
const segments$1 = [].concat(
  ...rectangleSegments$1, worldSkeleton$1.segments
)
const rectanglePoints$1 = rectangles.map(
  rectangle => rectangle.points
)
const points$1 = [].concat(
  ...rectanglePoints$1, worldSkeleton$1.points
)

const canvas = document.getElementById('canvas')
const context = canvas.getContext('2d')
context.font = '5px Arial'

const drawCircle = (position, radius, color) => {
  context.beginPath()
  context.arc(
    position.x,
    position.y,
    radius,
    0,
    2 * Math.PI,
    false
  )
  context.fillStyle = color
  context.fill()
  context.closePath()
}

const drawLine = (start, end, color, stroke) => {
  context.beginPath()
  context.moveTo(start.x, start.y)
  context.lineTo(end.x, end.y)
  context.strokeStyle = color
  context.lineWidth = stroke
  context.stroke()
  context.closePath()
}

const drawRectangle = (position, width, height, color) => {
  context.fillStyle = color
  context.fillRect(
    position.x - (width / 2),
    position.y - (height / 2),
    width,
    height
  )
}

const resetContext = (context) => {
  context.setTransform(1, 0, 0, 1, 0, 0)
  context.scale(
    canvas.width / CANVAS_WIDTH, canvas.height / CANVAS_HEIGHT
  )
}

const worldSkeleton = getSkeleton(ZERO_VECTOR, WORLD_HALF_SIZE)
const rectangleSegments = rectangles.map(
  rectangle => rectangle.segments
)
const segments = [].concat(
  ...rectangleSegments, worldSkeleton.segments
)
const rectanglePoints = rectangles.map(
  rectangle => rectangle.points
)
const points = [].concat(
  ...rectanglePoints, worldSkeleton.points
)

const draw = () => {
  resetContext(context)
  context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)

  // Center the camera
  camera.position = camera.subject.draw
  camera.angle = camera.subject.drawAngle
  const subject = camera.subject

  // Draw the darkness
  drawRectangle(
    CANVAS_HALF_SIZE,
    CANVAS_WIDTH,
    CANVAS_HEIGHT,
    DARK_GRAY
  )

  // Move the context
  context.translate(CANVAS_HALF_WIDTH, CANVAS_HEIGHT * 0.9)
  context.rotate(-camera.angle - Math.PI / 2)
  context.translate(-camera.position.x, -camera.position.y)

  // Get angles
  const angleGroups = points.map(point => {
    const angle = getAngle(subject.position, point)
    return [angle - 0.00001, angle, angle + 0.00001]
  })
  const uniqueAngles = [].concat(...angleGroups)

  // Find the intersections
  const intersections = []
  uniqueAngles.map(angle => {
    // Calculate a ray from the subject
    const ray = getUnitRay(subject.position, angle)

    // Find the closest intersection
    const segmentIntersections = []
    segments.map(segment => {
      const intersection = getIntersection(ray, segment)
      if (intersection) {
        segmentIntersections.push(intersection)
      }
    })
    const closest = segmentIntersections.sort(
      (a, b) => a.param - b.param
    )[0]

    // Store the intersection
    if (closest) {
      closest.angle = angle
      intersections.push(closest)
    }
  })

  // Sort intersects by angle
  const intersectionsByAngle = intersections.sort(
    (a, b) => a.angle - b.angle
  )

  // Draw the polygon
  context.beginPath()
  context.moveTo(intersections[0].x, intersections[0].y)
  intersectionsByAngle.map(
    intersection => context.lineTo(
      intersection.x, intersection.y
    )
  )
  context.fillStyle = WHITE
  context.fill()
  context.closePath()

  // Draw the attacks
  combatants.map(combatant => {
    if (combatant.attack.ray.length > 0) {
      if (combatant.attack.remaining > 37) {
        const colors = {
          40: FIRE_YELLOW,
          39: FIRE_YELLOW,
          38: YELLOW,
          37: YELLOW
        }
        drawLine(
          combatant.attack.ray[0],
          combatant.attack.hit.point,
          colors[combatant.attack.remaining],
          0.5
        )
      }
    }
  })

  circles.map(circle => {
    const visible = isVisible(
      subject.position,
      circle.position,
      subject.angle,
      circle.radius,
      segments
    )

    // If visible, draw the circle
    if (visible) {
      drawCircle(circle.draw, circle.radius, circle.color)

      // Draw the weapon
      if (circle.brain.ai.name !== 'civilian') {
        const directionVector = getDirection(
          circle.drawAngle
        )
        const end = vectorDoTwo(
          circle.draw,
          directionVector,
          (a, b) => a + circle.weaponSize * b
        )
        drawLine(circle.draw, end, BLACK, 0.5)
      }
    }

    // Smooth movement
    circle.draw = lerpVector(circle.draw, circle.position, 0.5)
    circle.draw = circle.position
    circle.drawAngle = circle.angle
    circle.drawAngle = lerp(
      circle.drawAngle, circle.angle, 0.9
    )
  })

  // Draw each rectangle
  rectangles.map(rectangle => {
    drawRectangle(
      rectangle.draw,
      rectangle.width,
      rectangle.height,
      rectangle.color
    )

    // Smooth movement
    rectangle.draw = lerpVector(
      rectangle.draw, rectangle.position, 0.5
    )
    rectangle.draw = rectangle.position
  })

  if (DEBUG) {
    // Draw the debug segments
    segments.map(segment => {
      drawLine(segment.a, segment.b, SKY_BLUE, 0.5)
    })

    // Draw debug lines
    intersections.map(intersection => {
      drawLine(subject.position, intersection, RED, 0.1)
      drawCircle(intersection, 0.5, GREEN)
    })
  }

  resetContext(context)

  // Draw the cooldown
  const remaining = Math.ceil(
    subject.attack.remaining / UPDATE_SECOND * 10
  ) / 10
  if (remaining === 0) {
    context.fillStyle = GREEN
  } else {
    context.fillStyle = RED
    // Draw the indicator
    const width = 20 * (
      subject.attack.remaining / subject.attack.cooldown
    )
    context.fillRect(20, 6, width, 5)
  }
  context.fillText(remaining, 10, 10)

  // Draw the score
  context.fillStyle = BLACK
  context.fillText(subject.score, 151.8, 10)

  window.requestAnimFrame(() => draw())
}

const getRandomTime = (seconds) => (
  Math.random() * UPDATE_SECOND * 30
)
const getRandomAngle = () => Math.random() * TWO_PI

const planRandomJourney = (ai) => {
  if (ai.roamTime < 0) {
    return Object.assign(
      ai,
      {
        roamAngle: getRandomAngle(),
        roamTime: getRandomTime(30)
      }
    )
  }

  return ai
}

const turnTowards = (a, b, input) => {
  if (a % TWO_PI > b) {
    return Object.assign(input, { right: false, left: true })
  } else {
    return Object.assign(input, { right: true, left: false })
  }
}

const roam = (object) => {
  const newAi = planRandomJourney(object.brain.ai)

  const newInput = turnTowards(
    object.angle, newAi.roamAngle, object.brain.input
  )

  return {
    ai: Object.assign(
      newAi, { roamTime: newAi.roamTime - 1 }
    ),
    input: Object.assign(
      newInput, { forward: true }
    )
  }
}

const fire = (brain) => ({
  ai: Object.assign(brain.ai, { aimTime: 0 }),
  input: Object.assign(brain.input, { aim: false, fire: true })
})

const misfire = (brain) => {
  if (brain.input.aim) { return fire(brain) }
  return brain
}

const aim = (brain) => ({
  ai: Object.assign(
    brain.ai, { aimTime: brain.ai.aimTime + 1 }
  ),
  input: Object.assign(brain.input, { aim: true, fire: false })
})

const face = (input, a, b) => turnTowards(
  a.angle, getAngle(a.position, b.position), input
)

const hunt = (object, hostiles, segments) => {
  const hunter = Object.assign({}, object)

  // Find living, visible hostiles
  const enemies = hostiles.filter(roamer => {
    const visible = isVisible(
      hunter.position,
      roamer.position,
      hunter.angle,
      roamer.radius,
      segments
    )

    return visible && roamer.color !== BLACK
  })

  // Check if any valid enemies were found
  if (enemies.length > 0) {
    // If so, stop roaming
    hunter.brain.ai.roamTime = 0

    // Find the closest enemy
    const closest = getClosest(hunter, enemies)
    const enemy = closest.object

    // Turn towards the enemy
    hunter.brain.input = face(
      hunter.brain.input, hunter, enemy
    )

    // Check if the enemy is close enough to shoot
    const distance = closest.distance
    if (distance > 50) {
      // // If not, chase it
      // Stop aiming
      hunter.brain = misfire(hunter.brain)

      // Move forward
      hunter.brain.input.forward = true
    } else {
      // If so, check if the hunter is reloading
      const attack = hunter.attack
      if (attack.remaining === 0) {
        // If not, check if the hunter has aimed long enough
        const range = (
          hunter.radius + hunter.brain.ai.aimTime * hunter.attack.accuracy
        )
        if (range >= distance) {
          // If so, fire
          hunter.brain = fire(hunter.brain)
        } else {
          // If not, aim
          hunter.brain = aim(hunter.brain)
        }
      }
    }
  } else {
    // If not, stop aiming
    hunter.brain = misfire(hunter.brain)

    // Roam around
    hunter.brain = roam(hunter)
  }

  return hunter.brain
}

const update = () => {
  // Think for the roamers
  roamers.map(roamer => (roamer.brain = roam(roamer)))

  // Think for the hunter
  hunter.brain = hunt(hunter, roamers, segments$1)

  // Move the circles
  circles.map(circle => {
    // Turn
    if (circle.brain.input.right) { circle.angle += 0.05 }
    if (circle.brain.input.left) { circle.angle -= 0.05 }

    // Move
    if (!circle.brain.input.aim) {
      circle.velocity = ZERO_VECTOR
      const forward = getDirection(circle.angle)
      const right = getDirection(circle.angle + Math.PI / 2)

      if (circle.brain.input.backward) {
        circle.velocity = (
          vectorSubtract(circle.velocity, forward)
        )
      }
      if (circle.brain.input.forward) {
        circle.velocity = vectorAdd(circle.velocity, forward)
      }
      if (circle.brain.input.strafeLeft) {
        circle.velocity = (
          vectorSubtract(circle.velocity, right)
        )
      }
      if (circle.brain.input.strafeRight) {
        circle.velocity = vectorAdd(circle.velocity, right)
      }

      circle.velocity = getUnitVector(circle.velocity)
      circle.position = vectorDoTwo(
        circle.position,
        circle.velocity,
        (a, b) => a + (b * circle.movementRate)
      )
    }
  })

  // Bounce
  unfixedCircles.map((circle, index) => {
    circles.map((innerCircle, innerCircleIndex) => {
      if (index !== innerCircleIndex) {
        const distance = getDistance(circle.position, innerCircle.position)
        if (distance < circle.radius + innerCircle.radius) {
          const unitVector = getUnitVector(
            vectorSubtract(
              circle.position, innerCircle.position
            )
          )

          circle.position = vectorDoTwo(
            circle.position,
            unitVector,
            (a, b) => a + b * MAXIMUM_SPEED
          )

          if (!innerCircle.fixed) {
            innerCircle.position = vectorDoTwo(
              innerCircle.position,
              unitVector,
              (a, b) => a - b * MAXIMUM_SPEED
            )
          }
        }
      }
    })

    rectangles.map(
      (rectangle) => circleRectangleBounce(circle, rectangle)
    )
  })

  unfixedRectangles.map((rectangle, index) => {
    rectangles.map((innerRectangle, innerRectangleIndex) => {
      if (index !== innerRectangleIndex) {
        const collisionViolation = rectangleCollision(
          rectangle, innerRectangle
        )
        if (collisionViolation) {
          rectangleBounce(
            collisionViolation, rectangle, innerRectangle
          )
        }
      }
    })

    fixedCircles.map(
      (circle) => circleRectangleBounce(circle, rectangle)
    )
  })

  combatants.map(combatant => {
    const attack = combatant.attack
    const input = combatant.brain.input
    attack.remaining = Math.max(
      0, attack.remaining - 1
    )
    if (attack.remaining === 0) {
      if (input.aim) {
        if (attack.charge === 0) { aimSound.play() }
        attack.charge += attack.accuracy
      } else if (input.fire) {
        // Reset the attack
        input.fire = false
        attack.remaining = attack.cooldown

        // Play the sound
        attack.attackSound.play()
        attack.playReloadSound()

        // Check if anything is hit
        const direction = getDirection(combatant.angle)
        const start = vectorDoTwo(
          combatant.position,
          direction,
          (a, b) => a + (b * (combatant.radius + 0.01))
        )
        const sequence = getSequence(0, attack.charge, 1)
        attack.ray = getRay(start, combatant.angle, sequence)
        attack.hit = rayCast(attack.ray, circles, rectangles)

        if (attack.hit.object && !attack.hit.object.fixed) {
          const object = attack.hit.object
          const name = object.brain.ai.name
          hitSound.play()

          if (name === 'roamer' && object.color !== BLACK) {
            combatant.score += 1
          } else if (name === 'civilian') {
            civilianHitSound.play()
          }
          object.color = BLACK
        }

        attack.charge = 0
      }
    } else {
      if (input.aim) { misfireSound.play() }
      input.fire = false
      attack.charge = 0
    }
  })
}

// Handle keyboard input.
const keys = {
  37: 'left', // left
  38: 'forward', // up
  39: 'right', // right
  40: 'backward', // down
  65: 'left', // a
  68: 'right', // d
  69: 'strafeRight', // e
  74: 'strafeLeft', // j
  75: 'strafeRight', // k
  79: 'strafeLeft', // o
  80: 'strafeRight', // p
  81: 'strafeLeft', // q
  83: 'backward', // s
  87: 'forward', // w
  32: 'attack' // space
}

const handleKeyEvent = (event, value) => {
  const key = keys[event.keyCode]
  if (key) {
    if (key === 'attack') {
      if (value) {
        player.brain.input.fire = false
        player.brain.input.aim = true
      } else {
        player.brain.input.aim = false
        player.brain.input.fire = true
      }
    }
    player.brain.input[key] = value
  }
}

window.requestAnimFrame = ((callback) => {
  return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.oRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  ((callback) => { window.setTimeout(callback, 1000 / 60) })
})()

window.onkeydown = (event) => handleKeyEvent(event, true)
window.onkeyup = (event) => handleKeyEvent(event, false)

const init = () => {
  draw()
  setInterval(update, UPDATED_RATE)
}

init()
},{}]},{},[1]);
