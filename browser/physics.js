import { MAXIMUM_SPEED } from './constants'

const add = (a, b) => a + b
const subtract = (a, b) => a - b
const divide = (a, b) => a / b
const multiply = (a, b) => a * b
const difference = (a, b) => Math.abs(a - b)
export const square = (a) => a * a

export const vectorDo = (vector, fn) => (
  { x: fn(vector.x), y: fn(vector.y) }
)
export const vectorDoTwo = (a, b, fn) => (
  { x: fn(a.x, b.x), y: fn(a.y, b.y) }
)
export const vectorDoThree = (a, b, c, fn) => (
  { x: fn(a.x, b.x, c.x), y: fn(a.y, b.y, c.y) }
)

export const vectorAdd = (a, b) => vectorDoTwo(a, b, add)
export const vectorSubtract = (a, b) => vectorDoTwo(
  a, b, subtract
)
export const vectorDivide = (a, b) => vectorDoTwo(a, b, divide)
export const vectorMultiply = (a, b) => vectorDoTwo(
  a, b, multiply
)
export const vectorDifference = (a, b) => vectorDoTwo(
  a, b, difference
)

export const times = (fn, number, current = 0) => {
  if (current < number) {
    fn()
    times(fn, number, current + 1)
  }
}

export const getLength = (vector) => Math.sqrt(square(vector.x) + square(vector.y))

export const getDistance = (a, b) => getLength(
  vectorSubtract(a, b)
)

export const getUnitVector = (vector) => {
  const vectorLength = getLength(vector)
  if (vectorLength === 0) return vector

  return vectorDo(vector, (value) => value / vectorLength)
}

export const getDirection = (angle) => (
  { x: Math.cos(angle), y: Math.sin(angle) }
)

export const getSequence = (start, end, step) => {
  const array = []
  array[0] = start
  while (array[array.length - 1] < end) {
    array[array.length] = array[array.length - 1] + step
  }
  if (array[array.length - 1] !== end) {
    array[array.length] = end
  }
  return array
}

export const getRay = (point, angle, sequence) => {
  const directionVector = getDirection(angle)
  return sequence.map((element) => vectorDoTwo(
    point, directionVector, (a, b) => a + element * b
  ))
}

export const rectangleCollision = (a, b) => {
  const difference = vectorDifference(a.position, b.position)
  const violation = vectorDoThree(
    a.halfSize,
    b.halfSize,
    difference,
    (a, b, c) => a + b - c
  )

  if (violation.x > 0 && violation.y > 0) {
    return violation
  } else {
    return null
  }
}

export const getRandomPosition = (vector) => vectorDo(
  vector, (a) => Math.random() * a - (a / 2)
)

export const getSkeleton = (position, halfSize) => {
  const points = [
    // Upper left
    { x: position.x - halfSize.x, y: position.y + halfSize.y },
    // Upper right
    { x: position.x + halfSize.x, y: position.y + halfSize.y },
    // Lower left
    { x: position.x - halfSize.x, y: position.y - halfSize.y },
    // Lower Right
    { x: position.x + halfSize.x, y: position.y - halfSize.y }
  ]
  return {
    points,
    segments: [
      { a: points[0], b: points[1] }, // Top
      { a: points[1], b: points[3] }, // Right
      { a: points[2], b: points[3] }, // Bottom
      { a: points[0], b: points[2] } // Left
    ]
  }
}

export const lerp = (a, b, weight) => (
  weight * a + (1 - weight) * b
)
export const lerpVector = (a, b, weight) => (
  vectorDoTwo(
    a, b, (a, b) => weight * a + (1 - weight) * b
  )
)

export const getAngle = (a, b) => Math.atan2(b.y - a.y, b.x - a.x)

// TODO: check name
export const getUnitRay = (a, angle) => ({
  a: a,
  b: vectorAdd(a, { x: Math.cos(angle), y: Math.sin(angle) })
})

export const getIntersection = (ray, segment) => {
  // Are they parallel? If so, no intersect
  const rayVector = getUnitVector(vectorSubtract(ray.b, ray.a))
  const segmentVector = vectorSubtract(segment.b, segment.a)
  const rayLength = getLength(rayVector)
  const segmentLength = getLength(segmentVector)

  // TODO: Break into variables and use vectorDo
  if (
    rayVector.x / rayLength ===
    segmentVector.x / segmentLength &&
    rayVector.y / rayLength ===
    segmentVector.y / segmentLength
  ) {
    // Unit vectors are the same.
    return null
  }

  // TODO: Break into variables
  // Solve for t1 and t2
  const t2 = (
    (
      rayVector.x * (segment.a.y - ray.a.y) +
      rayVector.y * (ray.a.x - segment.a.x)
    ) /
    (
      segmentVector.x * rayVector.y -
      segmentVector.y * rayVector.x
    )
  )

  // TODO: Break into variables
  const t1 = (
    segment.a.x + segmentVector.x * t2 - ray.a.x
  ) / rayVector.x

  // TODO: specify 'whatevers'
  // Must be within parametic whatevers for RAY/SEGMENT
  if (t1 < 0) return null
  if (t2 < 0 || t2 > 1) return null

  // Return the point of intersection
  return {
    x: ray.a.x + rayVector.x * t1,
    y: ray.a.y + rayVector.y * t1,
    param: t1
  }
}

export const isIntersected = (a, b, segments) => {
  const angle = getAngle(a, b)
  const ray = getUnitRay(a, angle)
  const distance = getLength(vectorSubtract(a, b))

  return segments.some(segment => {
    const intersection = getIntersection(ray, segment)
    return intersection && intersection.param < distance
  })
}

export const isVisible = (a, b, angle, radius, segments) => {
  const centerIntersected = isIntersected(a, b, segments)
  if (!centerIntersected) { return true }

  const rightDirection = getDirection(
    angle + Math.PI / 2
  )
  const rightOffset = vectorDo(rightDirection, a => a * radius)
  const rightPosition = vectorAdd(b, rightOffset)
  const rightIntersected = isIntersected(
    a, rightPosition, segments
  )
  if (!rightIntersected) { return true }

  const leftOffset = vectorDo(rightOffset, a => a * -1)
  const leftPosition = vectorAdd(b, leftOffset)
  const leftIntersected = isIntersected(
    a, leftPosition, segments
  )
  if (!leftIntersected) { return true }

  return false
}

const hitCheckPoint = (point, circles, rectangles) => {
  const circle = circles.find(
    (circle) => (
      getDistance(circle.position, point) < circle.radius
    )
  )
  return circle || rectangles.find((rectangle) => {
    const difference = vectorDifference(
      point, rectangle.position
    )
    const violation = vectorSubtract(
      rectangle.halfSize, difference
    )
    return violation.x > 0 && violation.y > 0
  })
}

export const rayCast = (ray, circles, rectangles) => {
  const pointer = {
    point: ray[ray.length - 1],
    object: null
  }
  ray.some((point) => {
    const hit = hitCheckPoint(point, circles, rectangles)
    if (hit) {
      pointer.point = point
      pointer.object = hit

      return true
    }
  })

  return pointer
}

export const rectangleBounce = (violation, a, b) => {
  if (violation.y > violation.x) {
    if (a.position.x > b.position.x) {
      if (!a.fixed) a.position.x += MAXIMUM_SPEED
      if (!b.fixed) b.position.x -= MAXIMUM_SPEED
    } else {
      if (!a.fixed) a.position.x -= MAXIMUM_SPEED
      if (!b.fixed) b.position.x += MAXIMUM_SPEED
    }
  } else {
    if (a.position.y > b.position.y) {
      if (!a.fixed) a.position.y += MAXIMUM_SPEED
      if (!b.fixed) b.position.y -= MAXIMUM_SPEED
    } else {
      if (!a.fixed) a.position.y -= MAXIMUM_SPEED
      if (!b.fixed) b.position.y += MAXIMUM_SPEED
    }
  }
}

export const circleRectangleBounce = (circle, rectangle) => {
  const difference = vectorDifference(
    circle.position, rectangle.position
 )
  const violation = vectorDoTwo(
    rectangle.halfSize,
    difference,
    (a, b) => circle.radius + a - b
  )
  const sideCase = (
    Math.max(violation.x, violation.y) > circle.radius
  )
  const sideCollision = (
    violation.x > 0 && violation.y > 0 && sideCase
  )

  const diagonal = {
    x: rectangle.halfSize.x,
    y: -rectangle.halfSize.y
  }
  const corners = [
    vectorAdd(rectangle.position, diagonal),
    vectorSubtract(rectangle.position, diagonal),
    vectorAdd(rectangle.position, rectangle.halfSize),
    vectorSubtract(rectangle.position, rectangle.halfSize)
  ].map(corner => getDistance(corner, circle.position))
  const cornerDistance = Math.min(...corners)

  const collision = (
    sideCollision || (cornerDistance < circle.radius)
  )

  if (collision) {
    rectangleBounce(violation, circle, rectangle)
  }
}

export const getClosest = (a, objects) => {
  const distances = objects.map(
    (object, index) => ({
      distance: getDistance(
        a.position, object.position
      ),
      index: index
    })
  )
  const closest = distances.sort(
    (a, b) => a.distance - b.distance
  )[0]

  return {
    object: objects[closest.index],
    distance: closest.distance
  }
}
