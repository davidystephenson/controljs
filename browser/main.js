import { draw } from './draw'
import { update } from './update'
import { UPDATED_RATE } from './constants'
import { handleKeyEvent } from './input'

window.requestAnimFrame = ((callback) => {
  return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.oRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  ((callback) => { window.setTimeout(callback, 1000 / 60) })
})()

window.onkeydown = (event) => handleKeyEvent(event, true)
window.onkeyup = (event) => handleKeyEvent(event, false)

const init = () => {
  draw()
  setInterval(update, UPDATED_RATE)
}

init()
