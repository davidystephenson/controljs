import {
  MINIMUM_SPEED, MAXIMUM_SPEED, WORLD_WIDTH, WORLD_HEIGHT,
  WORLD_HALF_WIDTH, WORLD_HALF_HEIGHT, MINIMUM_SIZE, RED,
  MINIMUM_RADIUS, UPDATE_SECOND, GREEN, PURPLE, SKY_BLUE,
  ZERO_VECTOR, WORLD_HALF_SIZE, UPDATED_RATE
} from './constants'
import { leeEnfieldSound, leeEnfieldReloadSound } from './sounds'
import { createRectangle, rectangles } from './rectangles'
import { createCircle, createRandomCircle } from './circles'
import { times, getSkeleton } from './physics'
import { createRandomObject } from './objects'

export const roamers = []
const roamerAi = {
  roamAngle: 0,
  getAngle () { return Math.random() * Math.PI * 2 },
  getTime () { return Math.random() * UPDATE_SECOND * 30 },
  name: 'roamer',
  roamTime: 0
}
const createRoamer = () => {
  const roamer = createRandomCircle()
  roamer.brain.ai = Object.assign({}, roamerAi)
  roamer.angle = roamer.brain.ai.getAngle()
  roamer.color = GREEN
  roamer.movementRate = MINIMUM_SPEED + Math.random() * MINIMUM_SPEED

  roamers.push(roamer)
  return roamer
}

const leeEnfield = {
  accuracy: 3,
  cooldown: UPDATE_SECOND * 2,
  charge: 0,
  hit: {},
  ray: [],
  reloadSound: leeEnfieldReloadSound,
  reloadSoundLength: 1265,
  remaining: 0,
  attackSound: leeEnfieldSound,
  time: 0
}
leeEnfield.playReloadSound = () => setTimeout(
  () => leeEnfield.reloadSound.play(),
  (leeEnfield.cooldown * UPDATED_RATE) - leeEnfield.reloadSoundLength
)

export const combatants = []
export const player = createCircle(0, 0, SKY_BLUE, MINIMUM_SIZE * 0.75, 0)
player.attack = Object.assign({}, leeEnfield)
player.brain.ai = { name: 'player' }
player.score = 0
player.movementRate = MAXIMUM_SPEED * 0.75
combatants.push(player)

export const hunter = createCircle(0, 0, RED, MINIMUM_SIZE * 0.75, 0)
hunter.attack = Object.assign({}, leeEnfield)
hunter.brain.ai = Object.assign(roamerAi, { name: 'hunter', aimTime: 0 })
hunter.score = 0
hunter.movementRate = MAXIMUM_SPEED * 0.75
combatants.push(hunter)

const houseSize = MINIMUM_SIZE * 10
const halfHouseSize = houseSize / 2
createRectangle(
  halfHouseSize - MINIMUM_RADIUS,
  halfHouseSize + MINIMUM_SIZE * 2,
  PURPLE,
  houseSize,
  houseSize,
  true
) // The House

const WALL_COLOR = PURPLE
const WALL_SIZE = 10
// Left wall
createRectangle(
  -WORLD_HALF_WIDTH + 5,
  0,
  WALL_COLOR,
  WALL_SIZE,
  WORLD_HEIGHT,
  true
)

// Right wall
createRectangle(
  WORLD_HALF_WIDTH + 5,
  0,
  WALL_COLOR,
  WALL_SIZE,
  WORLD_HEIGHT,
  true
)

// Top wall
createRectangle(
  0,
  WORLD_HALF_HEIGHT + 5,
  WALL_COLOR,
  WORLD_WIDTH + 20,
  10,
  true
)

// Bottom wall
createRectangle(
  0,
  -WORLD_HALF_HEIGHT - 5,
  WALL_COLOR,
  WORLD_WIDTH + 20,
  10,
  true
)

times(createRandomObject, 50)
times(createRoamer, 25)

// Initialize the camera
export const camera = {
  angle: 0,
  position: {
    x: 50,
    y: 50
  },
  subject: player
}
camera.angle = camera.subject.angle

const worldSkeleton = getSkeleton(ZERO_VECTOR, WORLD_HALF_SIZE)
const rectangleSegments = rectangles.map(
  rectangle => rectangle.segments
)
export const segments = [].concat(
  ...rectangleSegments, worldSkeleton.segments
)
const rectanglePoints = rectangles.map(
  rectangle => rectangle.points
)
export const points = [].concat(
  ...rectanglePoints, worldSkeleton.points
)
