import { UPDATE_SECOND, TWO_PI, BLACK } from './constants'
import { getAngle, isVisible, getClosest } from './physics'

const getRandomTime = (seconds) => (
  Math.random() * UPDATE_SECOND * 30
)
const getRandomAngle = () => Math.random() * TWO_PI

const planRandomJourney = (ai) => {
  if (ai.roamTime < 0) {
    return Object.assign(
      ai,
      {
        roamAngle: getRandomAngle(),
        roamTime: getRandomTime(30)
      }
    )
  }

  return ai
}

const turnTowards = (a, b, input) => {
  if (a % TWO_PI > b) {
    return Object.assign(input, { right: false, left: true })
  } else {
    return Object.assign(input, { right: true, left: false })
  }
}

export const roam = (object) => {
  const newAi = planRandomJourney(object.brain.ai)

  const newInput = turnTowards(
    object.angle, newAi.roamAngle, object.brain.input
  )

  return {
    ai: Object.assign(
      newAi, { roamTime: newAi.roamTime - 1 }
    ),
    input: Object.assign(
      newInput, { forward: true }
    )
  }
}

export const fire = (brain) => ({
  ai: Object.assign(brain.ai, { aimTime: 0 }),
  input: Object.assign(brain.input, { aim: false, fire: true })
})

export const misfire = (brain) => {
  if (brain.input.aim) { return fire(brain) }
  return brain
}

export const aim = (brain) => ({
  ai: Object.assign(
    brain.ai, { aimTime: brain.ai.aimTime + 1 }
  ),
  input: Object.assign(brain.input, { aim: true, fire: false })
})

export const face = (input, a, b) => turnTowards(
  a.angle, getAngle(a.position, b.position), input
)

export const hunt = (object, hostiles, segments) => {
  const hunter = Object.assign({}, object)

  // Find living, visible hostiles
  const enemies = hostiles.filter(roamer => {
    const visible = isVisible(
      hunter.position,
      roamer.position,
      hunter.angle,
      roamer.radius,
      segments
    )

    return visible && roamer.color !== BLACK
  })

  // Check if any valid enemies were found
  if (enemies.length > 0) {
    // If so, stop roaming
    hunter.brain.ai.roamTime = 0

    // Find the closest enemy
    const closest = getClosest(hunter, enemies)
    const enemy = closest.object

    // Turn towards the enemy
    hunter.brain.input = face(
      hunter.brain.input, hunter, enemy
    )

    // Check if the enemy is close enough to shoot
    const distance = closest.distance
    if (distance > 50) {
      // // If not, chase it
      // Stop aiming
      hunter.brain = misfire(hunter.brain)

      // Move forward
      hunter.brain.input.forward = true
    } else {
      // If so, check if the hunter is reloading
      const attack = hunter.attack
      if (attack.remaining === 0) {
        // If not, check if the hunter has aimed long enough
        const range = (
          hunter.radius + hunter.brain.ai.aimTime * hunter.attack.accuracy
        )
        if (range >= distance) {
          // If so, fire
          hunter.brain = fire(hunter.brain)
        } else {
          // If not, aim
          hunter.brain = aim(hunter.brain)
        }
      }
    }
  } else {
    // If not, stop aiming
    hunter.brain = misfire(hunter.brain)

    // Roam around
    hunter.brain = roam(hunter)
  }

  return hunter.brain
}
