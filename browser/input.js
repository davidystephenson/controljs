import { player } from './world'

// Handle keyboard input.
const keys = {
  37: 'left', // left
  38: 'forward', // up
  39: 'right', // right
  40: 'backward', // down
  65: 'left', // a
  68: 'right', // d
  69: 'strafeRight', // e
  74: 'strafeLeft', // j
  75: 'strafeRight', // k
  79: 'strafeLeft', // o
  80: 'strafeRight', // p
  81: 'strafeLeft', // q
  83: 'backward', // s
  87: 'forward', // w
  32: 'attack' // space
}

export const handleKeyEvent = (event, value) => {
  const key = keys[event.keyCode]
  if (key) {
    if (key === 'attack') {
      if (value) {
        player.brain.input.fire = false
        player.brain.input.aim = true
      } else {
        player.brain.input.aim = false
        player.brain.input.fire = true
      }
    }
    player.brain.input[key] = value
  }
}
