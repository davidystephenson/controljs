// Canvas
export const CANVAS_WIDTH = 161.8
export const CANVAS_HEIGHT = 100
export const CANVAS_HALF_WIDTH = CANVAS_WIDTH / 2
export const CANVAS_HALF_HEIGHT = CANVAS_HEIGHT / 2
export const CANVAS_SIZE = {
  x: CANVAS_WIDTH, y: CANVAS_HEIGHT
}
export const CANVAS_HALF_SIZE = {
  x: CANVAS_HALF_WIDTH, y: CANVAS_HALF_HEIGHT
}

// World
export const WORLD_WIDTH = 500
export const WORLD_HEIGHT = 500
export const WORLD_HALF_WIDTH = WORLD_WIDTH / 2
export const WORLD_HALF_HEIGHT = WORLD_HEIGHT / 2
export const WORLD_SIZE = { x: WORLD_WIDTH, y: WORLD_HEIGHT }
export const WORLD_HALF_SIZE = { x: WORLD_HALF_WIDTH, y: WORLD_HALF_HEIGHT }

// Colors
export const RED = '#FF0000'
export const SKY_BLUE = '#0088FF'
export const BLUE = '#0000FF'
export const BLACK = '#000000'
export const LIME = '#00FF00'
export const GREEN = '#008000'
export const YELLOW = '#FFFF00'
export const PURPLE = '#7F007F'
export const FIRE_YELLOW = '#E5AD23'
export const WHITE = '#FFFFFF'
export const DARK_GRAY = '#a9a9a9'
export const DARK_GREY = DARK_GRAY

// Update
export const UPDATE_RATE = 50
export const UPDATE_RATE_MODIFIER = 1
export const UPDATED_RATE = UPDATE_RATE / UPDATE_RATE_MODIFIER
export const UPDATE_SECOND = 1000 / UPDATED_RATE

// Size
export const MINIMUM_SIZE = 5
export const MINIMUM_RADIUS = MINIMUM_SIZE / 2

// Velocity
export const MAXIMUM_SPEED = (MINIMUM_SIZE / 5) * UPDATE_RATE_MODIFIER
export const MINIMUM_SPEED = (MAXIMUM_SPEED / 2)

// Math
export const TWO_PI = Math.PI * 2
export const ZERO_VECTOR = { x: 0, y: 0 }

// Debug
export const DEBUG = false
