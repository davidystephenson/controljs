import { MINIMUM_SPEED, ZERO_VECTOR } from './constants'
import { createRandomCircle } from './circles'
import { createRandomRectangle } from './rectangles'

export const objects = []
export const fixedObjects = []
export const unfixedObjects = []
export const createObject = (x, y, color, fixed = false) => {
  const object = {
    angle: 0,
    color,
    draw: { x, y },
    fixed,
    movementRate: MINIMUM_SPEED,
    position: { x, y },
    velocity: ZERO_VECTOR
  }

  objects.push(object)
  fixed ? fixedObjects.push(object) : unfixedObjects.push(object)
  return object
}

export const createRandomObject = () => {
  const shape = Math.random()
  if (shape > 0.5) {
    return createRandomCircle()
  } else {
    return createRandomRectangle()
  }
}
