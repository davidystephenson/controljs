import {
  CANVAS_WIDTH, CANVAS_HEIGHT, CANVAS_HALF_WIDTH,
  CANVAS_HALF_SIZE, WORLD_HALF_SIZE, WHITE,
  FIRE_YELLOW, YELLOW, BLACK, UPDATE_SECOND, DARK_GRAY,
  DEBUG, SKY_BLUE, GREEN, RED, ZERO_VECTOR
} from './constants'

import { circles } from './circles'
import { rectangles } from './rectangles'
import { camera, combatants } from './world'
import {
  vectorDoTwo, getDirection, getSkeleton, lerp,
  lerpVector, getUnitRay, getIntersection, getAngle,
  isVisible
} from './physics'

const canvas = document.getElementById('canvas')
const context = canvas.getContext('2d')
context.font = '5px Arial'

const drawCircle = (position, radius, color) => {
  context.beginPath()
  context.arc(
    position.x,
    position.y,
    radius,
    0,
    2 * Math.PI,
    false
  )
  context.fillStyle = color
  context.fill()
  context.closePath()
}

const drawLine = (start, end, color, stroke) => {
  context.beginPath()
  context.moveTo(start.x, start.y)
  context.lineTo(end.x, end.y)
  context.strokeStyle = color
  context.lineWidth = stroke
  context.stroke()
  context.closePath()
}

const drawRectangle = (position, width, height, color) => {
  context.fillStyle = color
  context.fillRect(
    position.x - (width / 2),
    position.y - (height / 2),
    width,
    height
  )
}

const resetContext = (context) => {
  context.setTransform(1, 0, 0, 1, 0, 0)
  context.scale(
    canvas.width / CANVAS_WIDTH, canvas.height / CANVAS_HEIGHT
  )
}

const worldSkeleton = getSkeleton(ZERO_VECTOR, WORLD_HALF_SIZE)
const rectangleSegments = rectangles.map(
  rectangle => rectangle.segments
)
const segments = [].concat(
  ...rectangleSegments, worldSkeleton.segments
)
const rectanglePoints = rectangles.map(
  rectangle => rectangle.points
)
const points = [].concat(
  ...rectanglePoints, worldSkeleton.points
)

export const draw = () => {
  resetContext(context)
  context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)

  // Center the camera
  camera.position = camera.subject.draw
  camera.angle = camera.subject.drawAngle
  const subject = camera.subject

  // Draw the darkness
  drawRectangle(
    CANVAS_HALF_SIZE,
    CANVAS_WIDTH,
    CANVAS_HEIGHT,
    DARK_GRAY
  )

  // Move the context
  context.translate(CANVAS_HALF_WIDTH, CANVAS_HEIGHT * 0.9)
  context.rotate(-camera.angle - Math.PI / 2)
  context.translate(-camera.position.x, -camera.position.y)

  // Get angles
  const angleGroups = points.map(point => {
    const angle = getAngle(subject.position, point)
    return [angle - 0.00001, angle, angle + 0.00001]
  })
  const uniqueAngles = [].concat(...angleGroups)

  // Find the intersections
  const intersections = []
  uniqueAngles.map(angle => {
    // Calculate a ray from the subject
    const ray = getUnitRay(subject.position, angle)

    // Find the closest intersection
    const segmentIntersections = []
    segments.map(segment => {
      const intersection = getIntersection(ray, segment)
      if (intersection) {
        segmentIntersections.push(intersection)
      }
    })
    const closest = segmentIntersections.sort(
      (a, b) => a.param - b.param
    )[0]

    // Store the intersection
    if (closest) {
      closest.angle = angle
      intersections.push(closest)
    }
  })

  // Sort intersects by angle
  const intersectionsByAngle = intersections.sort(
    (a, b) => a.angle - b.angle
  )

  // Draw the polygon
  context.beginPath()
  context.moveTo(intersections[0].x, intersections[0].y)
  intersectionsByAngle.map(
    intersection => context.lineTo(
      intersection.x, intersection.y
    )
  )
  context.fillStyle = WHITE
  context.fill()
  context.closePath()

  // Draw the attacks
  combatants.map(combatant => {
    if (combatant.attack.ray.length > 0) {
      if (combatant.attack.remaining > 37) {
        const colors = {
          40: FIRE_YELLOW,
          39: FIRE_YELLOW,
          38: YELLOW,
          37: YELLOW
        }
        drawLine(
          combatant.attack.ray[0],
          combatant.attack.hit.point,
          colors[combatant.attack.remaining],
          0.5
        )
      }
    }
  })

  circles.map(circle => {
    const visible = isVisible(
      subject.position,
      circle.position,
      subject.angle,
      circle.radius,
      segments
    )

    // If visible, draw the circle
    if (visible) {
      drawCircle(circle.draw, circle.radius, circle.color)

      // Draw the weapon
      if (circle.brain.ai.name !== 'civilian') {
        const directionVector = getDirection(
          circle.drawAngle
        )
        const end = vectorDoTwo(
          circle.draw,
          directionVector,
          (a, b) => a + circle.weaponSize * b
        )
        drawLine(circle.draw, end, BLACK, 0.5)
      }
    }

    // Smooth movement
    circle.draw = lerpVector(circle.draw, circle.position, 0.5)
    circle.draw = circle.position
    circle.drawAngle = circle.angle
    circle.drawAngle = lerp(
      circle.drawAngle, circle.angle, 0.9
    )
  })

  // Draw each rectangle
  rectangles.map(rectangle => {
    drawRectangle(
      rectangle.draw,
      rectangle.width,
      rectangle.height,
      rectangle.color
    )

    // Smooth movement
    rectangle.draw = lerpVector(
      rectangle.draw, rectangle.position, 0.5
    )
    rectangle.draw = rectangle.position
  })

  if (DEBUG) {
    // Draw the debug segments
    segments.map(segment => {
      drawLine(segment.a, segment.b, SKY_BLUE, 0.5)
    })

    // Draw debug lines
    intersections.map(intersection => {
      drawLine(subject.position, intersection, RED, 0.1)
      drawCircle(intersection, 0.5, GREEN)
    })
  }

  resetContext(context)

  // Draw the cooldown
  const remaining = Math.ceil(
    subject.attack.remaining / UPDATE_SECOND * 10
  ) / 10
  if (remaining === 0) {
    context.fillStyle = GREEN
  } else {
    context.fillStyle = RED
    // Draw the indicator
    const width = 20 * (
      subject.attack.remaining / subject.attack.cooldown
    )
    context.fillRect(20, 6, width, 5)
  }
  context.fillText(remaining, 10, 10)

  // Draw the score
  context.fillStyle = BLACK
  context.fillText(subject.score, 151.8, 10)

  window.requestAnimFrame(() => draw())
}
