import {
  MAXIMUM_SPEED, ZERO_VECTOR, BLACK
} from './constants'
import {
  roamers, hunter, combatants, segments
} from './world'
import {
  circles, unfixedCircles, fixedCircles
} from './circles'
import { rectangles, unfixedRectangles } from './rectangles'
import {
  getDistance, vectorSubtract, vectorDoTwo,
  vectorAdd, getDirection, getUnitVector,
  rectangleCollision, getSequence, getRay,
  rayCast, circleRectangleBounce, rectangleBounce
} from './physics'
import {
  aimSound, hitSound, civilianHitSound, misfireSound
} from './sounds'
import { roam, hunt } from './brain'

export const update = () => {
  // Think for the roamers
  roamers.map(roamer => (roamer.brain = roam(roamer)))

  // Think for the hunter
  hunter.brain = hunt(hunter, roamers, segments)

  // Move the circles
  circles.map(circle => {
    // Turn
    if (circle.brain.input.right) { circle.angle += 0.05 }
    if (circle.brain.input.left) { circle.angle -= 0.05 }

    // Move
    if (!circle.brain.input.aim) {
      circle.velocity = ZERO_VECTOR
      const forward = getDirection(circle.angle)
      const right = getDirection(circle.angle + Math.PI / 2)

      if (circle.brain.input.backward) {
        circle.velocity = (
          vectorSubtract(circle.velocity, forward)
        )
      }
      if (circle.brain.input.forward) {
        circle.velocity = vectorAdd(circle.velocity, forward)
      }
      if (circle.brain.input.strafeLeft) {
        circle.velocity = (
          vectorSubtract(circle.velocity, right)
        )
      }
      if (circle.brain.input.strafeRight) {
        circle.velocity = vectorAdd(circle.velocity, right)
      }

      circle.velocity = getUnitVector(circle.velocity)
      circle.position = vectorDoTwo(
        circle.position,
        circle.velocity,
        (a, b) => a + (b * circle.movementRate)
      )
    }
  })

  // Bounce
  unfixedCircles.map((circle, index) => {
    circles.map((innerCircle, innerCircleIndex) => {
      if (index !== innerCircleIndex) {
        const distance = getDistance(circle.position, innerCircle.position)
        if (distance < circle.radius + innerCircle.radius) {
          const unitVector = getUnitVector(
            vectorSubtract(
              circle.position, innerCircle.position
            )
          )

          circle.position = vectorDoTwo(
            circle.position,
            unitVector,
            (a, b) => a + b * MAXIMUM_SPEED
          )

          if (!innerCircle.fixed) {
            innerCircle.position = vectorDoTwo(
              innerCircle.position,
              unitVector,
              (a, b) => a - b * MAXIMUM_SPEED
            )
          }
        }
      }
    })

    rectangles.map(
      (rectangle) => circleRectangleBounce(circle, rectangle)
    )
  })

  unfixedRectangles.map((rectangle, index) => {
    rectangles.map((innerRectangle, innerRectangleIndex) => {
      if (index !== innerRectangleIndex) {
        const collisionViolation = rectangleCollision(
          rectangle, innerRectangle
        )
        if (collisionViolation) {
          rectangleBounce(
            collisionViolation, rectangle, innerRectangle
          )
        }
      }
    })

    fixedCircles.map(
      (circle) => circleRectangleBounce(circle, rectangle)
    )
  })

  combatants.map(combatant => {
    const attack = combatant.attack
    const input = combatant.brain.input
    attack.remaining = Math.max(
      0, attack.remaining - 1
    )
    if (attack.remaining === 0) {
      if (input.aim) {
        if (attack.charge === 0) { aimSound.play() }
        attack.charge += attack.accuracy
      } else if (input.fire) {
        // Reset the attack
        input.fire = false
        attack.remaining = attack.cooldown

        // Play the sound
        attack.attackSound.play()
        attack.playReloadSound()

        // Check if anything is hit
        const direction = getDirection(combatant.angle)
        const start = vectorDoTwo(
          combatant.position,
          direction,
          (a, b) => a + (b * (combatant.radius + 0.01))
        )
        const sequence = getSequence(0, attack.charge, 1)
        attack.ray = getRay(start, combatant.angle, sequence)
        attack.hit = rayCast(attack.ray, circles, rectangles)

        if (attack.hit.object && !attack.hit.object.fixed) {
          const object = attack.hit.object
          const name = object.brain.ai.name
          hitSound.play()

          if (name === 'roamer' && object.color !== BLACK) {
            combatant.score += 1
          } else if (name === 'civilian') {
            civilianHitSound.play()
          }
          object.color = BLACK
        }

        attack.charge = 0
      }
    } else {
      if (input.aim) { misfireSound.play() }
      input.fire = false
      attack.charge = 0
    }
  })
}
