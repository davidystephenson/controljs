import {
  MAXIMUM_SPEED, MINIMUM_RADIUS, WORLD_SIZE, BLUE
} from './constants'
import { getRandomPosition } from './physics'
import { createObject } from './objects'

export const circles = []
export const fixedCircles = []
export const unfixedCircles = []

export const createCircle = (
  x, y, color, radius, angle, fixed = false
) => {
  const circle = createObject(x, y, color, fixed)
  circle.angle = angle
  circle.brain = {
    ai: { name: 'civilian' },
    input: {
      left: false,
      right: false,
      forward: false,
      backward: false,
      aim: false,
      fire: false,
      strafeLeft: false,
      strafeRight: false
    }
  }
  circle.drawAngle = angle
  circle.radius = radius
  circle.visibleCircles = []
  circle.weaponSize = circle.radius + MAXIMUM_SPEED

  circles.push(circle)
  if (fixed) {
    fixedCircles.push(circle)
  } else {
    unfixedCircles.push(circle)
  }

  return circle
}

export const createRandomCircle = () => {
  const position = getRandomPosition(WORLD_SIZE)
  const radius = (
    (Math.random() * MINIMUM_RADIUS) + MINIMUM_RADIUS
  )
  const angle = Math.random() * 2 * Math.PI

  return createCircle(
    position.x, position.y, BLUE, radius, angle
  )
}
