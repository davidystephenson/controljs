/* global Audio */

export const aimSound = new Audio(
  './public/sound/holdBreath.mp3'
)
export const leeEnfieldSound = new Audio(
  './public/sound/leeEnfield.mp3'
)
export const leeEnfieldReloadSound = new Audio(
  './public/sound/reload.wav'
)
export const misfireSound = new Audio(
  './public/sound/misfire.wav'
)
export const hitSound = new Audio(
  './public/sound/hit.mp3'
)
export const civilianHitSound = new Audio(
  './public/sound/civilianHit.mp3'
)
