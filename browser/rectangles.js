import { WORLD_SIZE, MINIMUM_SIZE, PURPLE } from './constants'
import { createObject } from './objects'
import { getRandomPosition, rectangleCollision, getSkeleton } from './physics'

export const rectangles = []
export const fixedRectangles = []
export const unfixedRectangles = []

export const createRectangle = (
  x,
  y,
  color,
  width,
  height,
  fixed = false
) => {
  const rectangle = createObject(x, y, color, fixed)
  rectangle.width = width
  rectangle.halfWidth = width / 2
  rectangle.height = height
  rectangle.halfHeight = height / 2
  rectangle.size = { x: width, y: height }
  rectangle.halfSize = { x: rectangle.halfWidth, y: rectangle.halfHeight }

  const skeleton = getSkeleton(
    rectangle.position, rectangle.halfSize
  )
  rectangle.points = skeleton.points
  rectangle.segments = skeleton.segments

  rectangles.push(rectangle)
  fixed ? fixedRectangles.push(rectangle) : unfixedRectangles.push(rectangle)
  return rectangle
}

export const createRandomRectangle = () => {
  const position = getRandomPosition(WORLD_SIZE)
  const width = Math.random() * 100 + MINIMUM_SIZE
  const height = Math.random() * 100 + MINIMUM_SIZE

  const maximumSize = MINIMUM_SIZE * 2

  // Check if there is an overlap
  const skeleton = {
    position,
    halfSize: {
      x: (width / 2) + maximumSize,
      y: (height / 2) + maximumSize
    }
  }

  const overlap = rectangles.some(
    rectangle => rectangleCollision(skeleton, rectangle)
  )

  if (overlap) {
    return createRandomRectangle()
  } else {
    return createRectangle(position.x, position.y, PURPLE, width, height, true)
  }
}
